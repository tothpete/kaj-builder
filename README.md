# KAJ Website builder

This project is a demonstration of a prototype of a simple website builder enabling a user to add custom blocks
to the website and configure the look to his liking.

Currently a user can add these blocks:
* Text
* Image
* Button
* Grid components(Two columns, Three columns)

Finally, a user can export the markup and styles and save it for his own use.


## Usage

This screen is what a user sees first:
![](https://i.imgur.com/WE4V1SV.jpg)

### Add a new block
To add a new block, hover below a block in a container that can be extended and look for the triangle marker:
![](https://i.imgur.com/vpC9OjC.png)

### Select a new block to add
As mentioned before, a user cans select multiple block types to add:
![](https://i.imgur.com/nno4kqH.png)

### Configure a block
Upon addition of a new block or on demand, a user can configure his block as per specified settings:
![](https://i.imgur.com/i0wHMIr.png)

## Exporting
To export a website's markup and styles, hit 'Export' button in the page header. Please mind that **exporting can take up to a minute**.
