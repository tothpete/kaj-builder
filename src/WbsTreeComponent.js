export default class WbsTreeComponent {
  constructor(componentName, children, settings, attrs) {
    this.componentName = componentName;
    this.children = children || [];
    this.settings = settings || {};
    this.attrs = attrs || {};
  }
}