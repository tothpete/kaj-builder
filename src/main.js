import Vue from 'vue'
import App from './App.vue'

import './assets/styles/style.scss';

import 'froala-editor/js/froala_editor.pkgd.min';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'font-awesome/css/font-awesome.css';
import 'froala-editor/css/froala_style.min.css';
import VueFroala from 'vue-froala-wysiwyg';
// import Vue from 'vue';
Vue.use(VueFroala);

Vue.config.productionTip = false

const app = new Vue({
  render: h => h(App),
  methods: {
    wbsUnselectAll() {
      const traverseChild = (child) => {
        child.$data.wbsSelected = false;

        child.$children.forEach((c) => {
          traverseChild(c);
        })
      }

      traverseChild(this)
    }
  },
  mounted() {
  }
}).$mount('#app')

// window.app = app;
