import WbsTreeComponent from './WbsTreeComponent';
import html2json from 'html2json';
const CSSOM = require('cssom');

export default class WbsHtmlParser {
  constructor(html) {
    this.html = html;
  }

  buildTree() {
    const traverse = function(currentNode) {
      let children = [];

      if (currentNode.child && currentNode.child.length > 0) {
        currentNode.child.forEach((c) => {
          if (!c.child || c.child.length == 0) {
            let newNode = new WbsTreeComponent();

            if (c.node == 'text') {
              newNode.componentName = 'WbsTNode';
              newNode.settings.content = c.text;
            } else {
              console.log('reached here dont know what to do');
              // element
            }
            // add southernmost
            children.push(newNode);
          } else {
            children.push(traverse(c))
          }
        });
      }

      let newNode = new WbsTreeComponent();
      newNode.componentName = WbsHtmlParser.determineComponent(currentNode.tag);
      newNode.children = children;

      if (currentNode.attr && currentNode.attr.style) {
        newNode.attrs.style = WbsHtmlParser.serializeCss(currentNode.attr.style.join(''));
      }

      return newNode;
    }

    const jsonTree = html2json.html2json(this.html);
    // debugger;

    return { name: 'root', children: jsonTree.child.map(c => traverse(c))}
  }

  static determineComponent(tag) {
    const tagsToComponents = {
      h1: 'WbsHeading',
      h2: 'WbsHeading',
      h3: 'WbsHeading',
      h4: 'WbsHeading',
      h5: 'WbsHeading',
      h6: 'WbsHeading',
      a: 'WbsLink',
      p: 'WbsParagraph',
      span: 'WbsSpan',
      em: undefined,
      strong: 'WbsStrong'
    }

    return tagsToComponents[tag];
  }

  static serializeCss(string) {
    let obj = {}, s = string.toLowerCase().replace(/-(.)/g, function (m, g) {
      return g.toUpperCase();
    }).replace(/;\s?$/g,"").split(/:|;/g);
    for (let i = 0; i < s.length; i += 2)
      obj[s[i].replace(/\s/g,"")] = s[i+1].replace(/^\s+|\s+$/g,"");
    return obj;
  }
}