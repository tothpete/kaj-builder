export default class WbsFactory {
  static build(componentName) {
    const components = {
      'WbsText': this.text,
      'WbsSection': this.section,
      'WbsImage': this.image,
      'WbsButton': this.button,
      'WbsGridTwoCols': this.gridTwoCols,
      'WbsGridThreeCols': this.gridThreeCols
    }

    return components[componentName].apply()
  }

  static text() {
    return {
      componentName: 'WbsText',
      children: [],
      settings: {},
      attrs: {}
    }
  }
  static section() {
    return {
      componentName: 'WbsSection',
      children: [],
      settings: {},
      attrs: {}
    }
  }
  static image() {
    return {
      componentName: 'WbsImage',
      children: [],
      settings: { wbsEditing: true },
      attrs: {}
    }
  }
  static button() {
    return {
      componentName: 'WbsButton',
      children: [],
      settings: { wbsEditing: true },
      attrs: {}
    }
  }
  static gridTwoCols() {
    return {
      componentName: 'WbsGrid',
      children: [
        {
          componentName: 'WbsGridCol',
          children: [
            { componentName: 'WbsContainer', children: [], settings: {}, attrs: {} }
          ],
          settings: {},
          attrs: {}
        },
        {
          componentName: 'WbsGridCol',
          children: [
            { componentName: 'WbsContainer', children: [], settings: {}, attrs: {} }
          ],
          settings: {},
          attrs: {}
        }
      ],
      settings: {},
      attrs: {}
    }
  }
  static gridThreeCols() {
    return {
      componentName: 'WbsGrid',
      children: [
        {
          componentName: 'WbsGridCol',
          children: [
            { componentName: 'WbsContainer', children: [], settings: {}, attrs: {} }
          ],
          settings: {},
          attrs: {}
        },
        {
          componentName: 'WbsGridCol',
          children: [
            { componentName: 'WbsContainer', children: [], settings: {}, attrs: {} }
          ],
          settings: {},
          attrs: {}
        },
        {
          componentName: 'WbsGridCol',
          children: [
            { componentName: 'WbsContainer', children: [], settings: {}, attrs: {} }
          ],
          settings: {},
          attrs: {}
        }
      ],
      settings: {},
      attrs: {}
    }
  }
}