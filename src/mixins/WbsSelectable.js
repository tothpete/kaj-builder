export default {
  data() {
    return {
      wbsSelected: false
    }
  },
  methods: {
    selectableClickEvent(e) {
      e.stopImmediatePropagation();

      const selected = this.wbsSelected;
      this.$root.wbsUnselectAll();
      this.wbsSelected = !selected;
    }
  },
  mounted() {
    this.$el.classList.add('wbs-selectable');

    this.$el.addEventListener("click", this.selectableClickEvent)
  },
  destroyed() {
    this.$el.classList.remove('wbs-selectable');
    this.$el.removeEventListener('click', this.selectableClickEvent);
  },
  watch: {
    wbsSelected(newVal) {
      if(newVal) {
        this.$el.classList.add('wbs-selected')
      } else {
        this.$el.classList.remove('wbs-selected')
      }
    }
  }
}
