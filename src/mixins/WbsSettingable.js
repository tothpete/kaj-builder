export default {
  props: ['wbsId', 'wbsNotEditing'],
  data() {
    return {
      wbsSettings: {}
    }
  },
  computed: {
    componentSettings() {
      const props = JSON.parse(JSON.stringify(this.$props)) || {};
      const data = this.$data && JSON.parse(JSON.stringify(this.$data.wbsSettings)) || {};
      return Object.assign(props, data)
    }
  },
  mounted() {
    if (this.$options.name == 'WbsTNode') return;
    this.$el.dataset.wbsId = this.componentSettings.wbsId;
    this.$el.classList.add('wbs-settingable');
  },
  destroyed() {
    if (this.$options.name == 'WbsTNode') return;
    this.$el.dataset.wbsId = '';
    this.$el.classList.remove('wbs-settingable');
  }
}