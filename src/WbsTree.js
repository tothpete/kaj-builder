import WbsTreeComponent from './WbsTreeComponent';
import shortid from 'shortid';

export default class WbsTree {
  static buildFromVue(rootVueComponent) {
    const buildComponent = (vueComponent) => {
      let children = [];

      if (vueComponent.$children && vueComponent.$children.length > 0) {
        vueComponent.$children.forEach((c) => {
          if (!c.$children || c.$children.length == 0) {
            const settings = JSON.parse(JSON.stringify(c.componentSettings || {}));
            let attrs = JSON.parse(JSON.stringify(c.$attrs));
            attrs.class = c.$vnode && c.$vnode.data.staticClass;
            attrs.style = c.$vnode && c.$vnode.data.staticStyle;
            children.push(new WbsTreeComponent(c.$options.name, [], settings, attrs))
          } else {
            children.push(buildComponent(c))
          }
        });
      }

      if (vueComponent.$options.name == 'WbsHeading') {
        // debugger
      }

      const settings = vueComponent.componentSettings || {};
      let attrs = JSON.parse(JSON.stringify(vueComponent.$attrs));
      attrs.class = vueComponent.$vnode && vueComponent.$vnode.data.staticClass;
      attrs.style = vueComponent.$vnode && vueComponent.$vnode.data.staticStyle;
      const root = new WbsTreeComponent(vueComponent.$options.name, children, settings, attrs);

      return root;
    }

    return new WbsTree(buildComponent(rootVueComponent));
  }

  static buildFromString(json_tree=JSON.stringify({})) {
    return new WbsTree(JSON.parse(json_tree));
  }

  constructor(root) {
    this.root = root;
    this.assignId(this.root);
  }

  allComponents() {
    let components = [];

    const traverse = function(node) {
      components.push(node);

      if (node.children && node.children.length > 0) {
        node.children.forEach(function(child) {
          traverse(child);
        })
      }
    }

    traverse(this.root);

    return components;
  }

  components() {
    const components = {};
    this.allComponents().forEach(c => { components[c.settings.wbsId] = c }, this);
    return components;
  }

  updateSettings(wbsId, settings) {
    const oldSettings = JSON.parse(JSON.stringify(this.components()[wbsId].settings));
    const newSettings = JSON.parse(JSON.stringify(settings));
    // debugger;
    this.components()[wbsId].settings = Object.assign(oldSettings, newSettings);
  }

  replaceChildren(wbsId, newChildren) {
    this.components()[wbsId].children = newChildren;
  }

  appendChildTo(wbsId, newChild) {
    this.assignId(newChild);
    this.components()[wbsId].children.push(newChild);
  }

  assignId(currentNode) {
    const that = this;

    if (currentNode.children && currentNode.children.length > 0) {
      currentNode.children.forEach(function(child) {
        that.assignId(child);
      })
    }

    if (!currentNode.settings.wbsId) {
      // if(!currentNode.settings) currentNode.settings = {};
      currentNode.settings.wbsId = shortid.generate();
    }
  }

  insertAfter(wbsId, newNode) {
    this.assignId(newNode);

    const traverse = (currentNode, wbsId, newNode) => {
      if (currentNode.children && currentNode.children.length > 0) {
        const childrenIds = currentNode.children.map(child => child.settings.wbsId);
        const indexOfEl = childrenIds.indexOf(wbsId);

        if (indexOfEl != -1) {
          currentNode.children.splice(indexOfEl+1, 0, newNode);
          return;
        }

        currentNode.children.forEach((child) => traverse(child, wbsId, newNode));
      }
    }

    traverse(this.root, wbsId, newNode);
  }

  remove(wbsId) {
    const traverse = (currentNode, wbsId) => {
      if (currentNode.children && currentNode.children.length > 0) {
        const childrenIds = currentNode.children.map(child => child.settings.wbsId);
        const indexOfEl = childrenIds.indexOf(wbsId);

        if (indexOfEl != -1) {
          currentNode.children.splice(indexOfEl, 1);
          return;
        }

        currentNode.children.forEach((child) => traverse(child, wbsId));
      }
    }

    traverse(this.root, wbsId);
  }

  to_json() {
    return JSON.parse(JSON.stringify(this.root));
  }
}